# Introduction to Functional Programming FS23


## Grading (Leistungsnachweis)

- Written final examination ("Semesterendprüfung SEP")
    - Can be written electronically (on one's own device)
    - Includes coding
    - Accounts for at least *80%* of the final assessment
- Mini projects
    - Mini projects are **optional**
    - Deliverable is a presentation at the end of the term (last lecture)
    - The grading only counts if it is better than the final examination (**"Bonussystem"**)
    - The grading accounts for (a maximum of) **20%** of the final assessment
    - Projects can be handed in by teams consisting of no more than four members
    - Registration is mandatory (https://forms.gle/9RFQP3cBBTUWfBBo7)

## References and Links

### Course Website

-  [Moodle](https://moodle.zhaw.ch/course/view.php?id=12713)

### Functional Programming General Links

- [Wikipedia](https://en.wikipedia.org/wiki/Functional_programming)
- [A Brief History of Functional Programming](http://www.cse.psu.edu/~gxt29//historyOfFP/historyOfFP.html#:~:text=the%20value%20twice!-,Early%20Functional%20Languages,the%20functional%20programming%20paradigm%20significantly.)
- [Why Functional Programming Matters (by John Hughes)](https://www.cs.kent.ac.uk/people/staff/dat/miranda/whyfp90.pdf)
- [Functional Programming For The Rest of Us (blog post)](https://www.defmacro.org/2006/06/19/fp.html#history)

### Haskell Links

- [Documentation](https://www.haskell.org/documentation/) containing many more links (Books, Courses, Tutorials, etc.).
- [The Haskell tool stack](https://docs.haskellstack.org/en/stable/README/)
- [Hackage](https://hackage.haskell.org/), the Haskell package repository
- [Hoogle](https://hoogle.haskell.org/), a Haskell API search engine, which allows you to search the Haskell libraries on Stackage by either function name, or by approximate type signature.
