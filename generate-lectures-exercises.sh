#!/bin/bash

echo "Compiling lecture notes"
cd Lectures

numdirs=(*/)
numdirs=${#numdirs[@]}
numParts=$((numdirs-1))

for n in $(seq 1 $numParts); do
    echo "**********************"
    echo "Building Lecture" ${n}
    echo "**********************"
    cd Part-${n}
    latexmk -pdf presentation-${n}*.tex || exit 1
    [ -f exercise-${n}.tex ] && (latexmk -pdf exercise-${n}.tex || exit 1)
    echo "Cleaning up"
    rm *.aux
    rm *.fdb_latexmk
    rm *.fls
    rm *.log
    rm *.nav
    rm *.out
    rm *.snm
    rm *.toc
    rm *.vrb
    echo "Migrating"
    mkdir -p ../../Output/Part-${n}
    mv presentation-${n}*.pdf ../../Output/Part-${n}
    [ -f exercise-${n}.pdf ] && mv exercise-${n}.pdf ../../Output/Part-${n}
    [ -f Code/Exercise-${n}-Solutions.hs ] && cp Code/Exercise-${n}-Solutions.hs ../../Output/Part-${n}/ExerciseSolutions.hs
    cd ..
done