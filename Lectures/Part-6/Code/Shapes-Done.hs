module Shape where

import Data.List
import Matrix (Matrix (Matrix), Vector, Point, invert, apply)

newtype Shape = Shape { inside :: Point -> Bool }

-- | Basic Shapes
empty :: Shape
empty = Shape $ const False

unitDisc :: Shape
unitDisc = Shape $ \(x, y) ->
    x^2 + y^2 <= 1

unitSq :: Shape
unitSq = Shape $ \(x, y) ->
    abs x <= 1 && abs y <= 1

-- | Manipulations

-- | Moving (trnaslation) a shape
translate :: Vector -> Shape -> Shape
translate (dx, dy) s = Shape $ \(x, y) ->
    inside s (x - dx, y - dy)

negate :: Shape -> Shape
negate s = Shape $ not . inside s


-- | Combining two shapes
combineBool
    :: (Bool -> Bool -> Bool)
    -> Shape
    -> Shape
    -> Shape
combineBool f s1 s2 = Shape $ \p -> f (f1 p) (f2 p)
    where
        f1 = inside s1
        f2 = inside s2

intersect :: Shape -> Shape -> Shape
intersect = combineBool (&&)

merge :: Shape -> Shape -> Shape
merge = combineBool (||)

minus :: Shape -> Shape -> Shape
minus = combineBool c
    where
        c b1 b2 = b1 && not b2

-- | Matrix transformations
transformM :: Matrix -> Shape -> Shape
transformM m s = Shape $ \p ->
    inside s $ apply m' p
    where
        m' = invert m

stretchX :: Float -> Shape -> Shape
stretchX r = transformM $ Matrix (r, 0) (0, 1)

stretchY :: Float -> Shape -> Shape
stretchY r = transformM $ Matrix (1, 0) (0, r)

stretch :: Float -> Shape -> Shape
stretch r = transformM $ Matrix (r, 0) (0, r)

flipX :: Shape -> Shape
flipX = transformM (Matrix (1, 0) (0, -1))

flipY :: Shape -> Shape
flipY = transformM (Matrix (-1, 0) (0, 1))

flip45 :: Shape -> Shape
flip45 = transformM (Matrix (0, 1) (1, 0))

flip0 :: Shape -> Shape
flip0 = transformM (Matrix (-1, 0) (0, -1))

rotate :: Float -> Shape -> Shape
rotate a = transformM $ Matrix
        (cos a, -(sin a))
        (sin a, cos a)

-- | Semantics

-- | Render
render :: Float -> Float -> Shape -> IO ()
render length height shape = writeFile "shape.txt" lines
    where
        draw p
            | inside shape p = ('#', p)
            | otherwise = (' ', p)

        breakLn (d, (x,y))
            | x == length = [d,'\n']
            | otherwise = [d]

        pixels =
            [ draw (x,y)
                | y <- [(-height)..height]
                , x <- [(-length)..length]
            ]

        lines = concatMap breakLn pixels

-- Examples

shape1 = translate (100, 100) $ stretch 10 unitDisc
shape2 = rotate 1 unitSq

shape3 = translate (100, 100) $ merge shape1 shape2

iShape = flipX $ merge
    (stretchY 2 unitSq)
    (translate (0, 5) unitDisc)

disc50 = stretch 50 unitDisc