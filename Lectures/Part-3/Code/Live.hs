sumL :: [Integer] -> Integer
sumL [] = 0
sumL (x:xs) = x + sumL xs

sumA :: Integer -> [Integer] -> Integer
sumA acc [] = acc
sumA acc (x:xs) = sumA (x+acc) xs

sumC :: (Integer -> Integer)  -> [Integer] -> Integer
sumC cont [] = cont 0
sumC cont (x:xs) = sumC (\y -> cont (x+y)) xs

mapL :: (a->b) -> [a] -> [b]
mapL f [] = []
mapL f (x:xs) = f x : mapL f xs

mapA :: [b] -> (a->b) -> [a] -> [b]
mapA acc f [] = reverse acc
mapA acc f (x:xs) = mapA (f x : acc) f xs

--mapC ::  -> (a->b) -> [a] -> [b]
mapC cont f [] = cont []
mapC cont f (x:xs) = mapC (\y -> cont (f x : y) ) f xs


data Tree a = Tree a [Tree a]



sumT :: Tree Integer -> Integer
sumT (Tree a ts) = a + sum (map sumT ts)


t :: Tree Integer
t = Tree 1 [Tree 2 [], Tree 3 []]





