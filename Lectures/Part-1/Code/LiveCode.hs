import Data.List


{--
{- Contents
    A short introduction to Haskell;
    The very basics
-}


-----------------------------------------------------------
-- Comments
-----------------------------------------------------------

-- a single line comment

{- a multiline comment
   {- can be nested -}
-}

{- Usually (from now on), we will try to adhere to the
    following conventuion/structure for multiline comments:
-}

{- Optional "title" of the comment
    Content of the comment possibly spaning multiple lines
    with length <= 60.
-}


-----------------------------------------------------------
-- Simple declarations
-----------------------------------------------------------

{- Declaring a variable
    Generally, in Haskell we define the type of a variable
    in a separate line before we define the variable.
-}

-- 'five' is the integer 5
five :: Integer
five = 5

{-
    'six' is the integer 6. The type 'Integer' can
    accomodate integers of arbitrary size
-}
six :: Integer
six = 6

-- 'four' is the float 4.0
four :: Float
four = 4


-----------------------------------------------------------
---- Numeric operations
-----------------------------------------------------------

seven :: Integer
seven = five + 2

theIntEight :: Integer
theIntEight = 3 + five

theFloatSeven :: Float
theFloatSeven = 3 + four

twentyFour :: Float
twentyFour = 3 * 2 ^ 3

{- Integer division
    'div' denotes integer division. Generally, it is
    possible to write a function between '`' to use infix
    notation. I order to define functions that are
    "natively" infix, you would use brackets '(...)' when
    defining the function/operator.
-}
intSevenByThree :: Integer
intSevenByThree = seven `div` 3

{- Division
    The "normal" division '/' is for "fractional numbers,
    e.g. floats.
-}
floatSevenByThree :: Float
floatSevenByThree = 7 / 3

{- Exercise
    Fill out the types where possible
    Check your solutions with the REPL.
-}


x :: Integer
x = 2 ^ 3

y :: Float
y = 2.0 ^ 3

a :: Integer
a = x + 5

b :: Float
b = y/2

--c :: ?
--c = y `div` 3



-----------------------------------------------------------
---- Booleans and boolean operations
-----------------------------------------------------------

true :: Bool
true = True

false :: Bool
false = False

alsoTrue :: Bool
alsoTrue = true || false

alsoFalse :: Bool
alsoFalse = true && false

moreTruth :: Bool
moreTruth = not alsoFalse


-----------------------------------------------------------
---- Strings and chars
-----------------------------------------------------------

aChar :: Char
aChar = 'a'

aString :: String
aString = "Happy new year"

greeting :: String
greeting = aString ++ " " ++ "2023"

greeting2 :: String
greeting2 = concat
    [ aString
    , " "
    , "2023"
    ]



{- Warning
    The operators are strongly typed; they can only digest
    values of the same type. The following declarations are
    thus rejected by the type checker:

    aString = "Happy new year"
    greeting = aString ++ " " ++ 2022

    five :: Int
    five = 5
    seven = five + 2.0
-}


-----------------------------------------------------------
---- Lists
-----------------------------------------------------------

{-
    We will often use `List` (linked lists), a generic
    container to hold multiple values of *the same type*.
-}

aListOfStrings :: [String]
aListOfStrings = [ "one", "two", "three" ]

aListOfInts :: [Integer]
aListOfInts = [ 1, 2, 3 ]

{- Exercise
    Fill out the type, check your solutions with the REPL.
-}

friendGroups :: [[String]]
friendGroups =
  [ ["Peter", "Anna", "Roman", "Laura"]
  , ["Anna","Reto"]
  , ["Christoph", "Mara", "Andrew"]
  ]

{- List comprehension
    Aside from declaring lists explicitly by writing down
    their element (as shown before), lists can also be
    declared by "list comprehension" and also as "ranges".
-}

someInts :: [Integer]
someInts = [1..15] -- range

nats :: [Integer]
nats = [0..]

someEvens :: [Integer]
someEvens = [ 2*x | x <- [-5..5]]

someOdds :: [Integer]
someOdds = [2*x+1 | x <- [-5..5]]

pairs :: [(Integer, Bool)]
pairs = [ (x,y) | x <- [0..5], y <- [True, False]]

-- {(x,y) | x <- {0..5}, y <- {0..5}, x <y , x > 1 }
lessThanPairs :: [(Integer,Integer)]
lessThanPairs = [ (x,y)
                  | x <- [0..5]
                  , y <- [0..5]
                  , x < y -- guard
                  , x > 1 -- second guard
                  ]

{- Exercise
    Use list comprehension to declare a list that contains
    all square numbers between 1 and 100.
-}
squares :: [Integer]
squares = [x^2 | x <- [1..10]]

{- Exercise
    Use list comprehension to declare a list that contains
    all  odd square numbers between 1 and 100.
-}
oddSquares :: [Integer]
oddSquares = [ x | x <- squares, y <- [0..49], x == 2*y+1 ]

{- Contents
    A short integerroduction to Haskell;
    On how to create simple functions.
-}

-----------------------------------------------------------
-- Elementary Functions
-----------------------------------------------------------

{- General Syntax for functions
    * Input on the left, output on the right.
    * Functions also have a type: input type on the left
      side, output type on the right side, arrow inbetween.
-}
inc1 :: Integer -> Integer
inc1 n = n + 1

{- Lambda notation
    Everything can also be written on the right side (lambda
    notation). In fact, `inc1` is just syntactic sugar for
    `inc2`.
-}
inc2 :: Integer -> Integer
inc2 = \n -> n + 1 

{- Function application
    To apply a function, write the function to the left of
    the input/argument (no brackets needed).
-}
two :: Integer
two = inc1 1

someGreeting :: String -> String
someGreeting person = "Hello " ++ person


{- Exercise
    Define a function `square`, that squares the given input
    and write down its type. Define the same function using
    the lambda notation.
-}
square :: Integer -> Integer
square x = x * x

squareLambda :: Integer -> Integer
squareLambda = \x -> x*x


-----------------------------------------------------------
-- Functions of Several Variables
-----------------------------------------------------------

{- Tupled Inputs
    'addTupled' is binary function, its output depends on 
    two separate integers.
-}
addTupled :: (Integer, Integer) -> Integer
addTupled (x, y) = x + y

xs = map (\x -> addTupled (2, x)) [1..10]

{- 
    'avg3Tupled' is a function that depends on three input
    variables.
-}
avg3Tupled :: (Float, Float, Float) -> Float
avg3Tupled ( x, y, z ) = (x + y + z) / 3

{- Evaluating with Tuples
    We evaluate a function of several variables by providing
    values *for each* input variabele.
-}
four :: Float
four = avg3Tupled ( 3, 4, 5 )

{- 
    If we want to evaluate a multivariable function before
    we have all inputs ready, we have to properly
    parametrize it. We can do this with curried functions.
    More on that later!
-}


{- A Curried Function
    'add' is the parametrized (a.k.a curried) version of
    `add`. It is a function that returns a (unary) function.
    
    Note that the following three lines are equivalent
    declarations:

    'add = \x -> \y -> x + y'

    'add x = \y -> x + y'

    'add x y = x + y'

    Also note that the parenthesis are not needed in the 
    declaration of the type of 'add'.
-}
add :: Int -> (Int -> Int)
add n m = n + m


{- Partial Application
    'add3' is the function that adds '3' to any given input.
    Thanks to currying, we can realize 'add3' as a special
    case of 'add'. Thanks to partial application, this 
    corresponds to a single function call.
-}
add3 :: Int -> Int
add3 = add 3

{- Exercise
    Declare a curried version of the function 'avg3Tupled'
    as a lambda term.
-}
avg3 :: Float -> Float -> Float -> Float
avg3 = \x -> (\y z -> (x+y+z)/3)

{- Exercise
    use the binary function '(++)' that concatenates strings
    to specify a function that prepends "=> " to any given
    input string. Use partial application
-}
prepArrow :: String -> String
prepArrow = ("=> " ++)

-- When calling this
-- > prepArrow "foo"
-- It should output the following
-- '=> foo'


-----------------------------------------------------------
-- Higher Order Functions
-----------------------------------------------------------

{- Function as Input
    'apply' is a function that accepts a function as input
    (and applies it to the remaining input)
-}
apply :: (a -> b) -> a -> b
apply f x = f x

{- Function as Input and Output
    'twice' is a function that accepts and returns a 
    function.
-}
twice :: (a -> a) -> (a -> a)
twice f x = f (f x)
-- twice f = f . f -- Alternatively use this syntax

{- Exercise
    Write a function `thrice` that applies a function three
    times.
-}
thrice :: (a -> a) -> a -> a
thrice f = f . (twice f)

{- Exercise
    Write a function 'compose' that accepts two functions
    and applies them to a given argument in order.
-}
compose :: (a -> b) -> (b -> c) -> a -> c
compose f g x = g (f x)

{- Exercise
    reimplement 'twice' and 'thrice' with as an application
    of the function 'compose'.
-}
twiceByComp :: (a -> a) -> a -> a
twiceByComp f = compose f f

thriceByComp :: (a -> a) -> a -> a
thriceByComp f = compose f (twice f)

{- List map 
    A often used higher function is ``mapping'' of lists.
    This function will apply a function (given as an 
    argument) to every element in a list (also given as an
    argument) and return a new list containig the changed 
    values. There are a lot of other functions to work
    with lists (and similar types). We will learn about 
    these functions later.
-}
mapping :: [Integer]
mapping = map (\n -> n + 1) [1, 2, 3]

{-| Exercise
    greet all friends using 'friends', 'map' and
    'greeting'. 
-}
friends :: [String]
friends =
    [ "Peter"
    , "Nina"
    , "Janosh"
    , "Reto"
    , "Adal"
    , "Sara"
    ]

greeting :: String -> String
greeting person = "Hello " ++ person

greetFriends :: [String]
greetFriends = map greeting friends


data Shape 
    = Circle Float
    | Rectangle Float Float


area :: Shape -> Float
area s = case s of
    Circle r -> r*r*pi
    Rectangle b l -> b*l


data BTree a
    = Node (BTree a) a (BTree a)
    | Leaf a 
    deriving Show

tree :: BTree Int
tree = Node (Leaf 2) 1 (Leaf 3)

tree2 :: BTree Int
tree2 = Node (Leaf 34) 1 tree

depth :: BTree a -> Int
depth t = case t of
    Node l _ r -> 1+ max (depth l) (depth r)
    Leaf _ -> 1


data Value 
    = Fix Int
    | Variable Int Int


iden :: a -> a
iden x = x

ifelse x y z u = if x == y then z else u

--}

flatSort :: Ord a => [[a]] -> [a]
--flatSort xss = concat (map sort xss)
--flatSort = concat . (map sort)
flatSort xss = case xss of
    [] -> []
    xs:rest -> (sort xs) ++ flatSort rest

initial :: Eq a => [a] -> [a] -> Bool
initial [] _ = True
initial _ [] = False  
initial (x:xs) (y:ys) = x == y && initial xs ys

sub :: Eq a => [a] -> [a] -> Bool
sub xs [] = xs == []
sub xs ys = initial xs ys || sub xs (tail ys)


many :: Int -> (a -> a) -> a -> a
many 0 _ x = x
many n f x = f (many (n-1) f x)

ex :: [String] -> String
ex = foldr (\a x -> "(" ++ a ++ "*" ++ x ++")") "start" 

--foldl (flip (:)) [] [2,3,4] 

-- foldl: "(((start*a)*b)*c)"
-- foldr: "(a*(b*(c*start)))"


data BTree a 
    = Node a (BTree a) (BTree a)
    | Leaf a
    deriving Eq

tree :: (a -> b -> b -> b) -> (a -> b) -> BTree a -> b
tree nd lf t = case t of
    Node a l r -> nd a (recurse l) (recurse r)
    Leaf a -> lf a
    where
        recurse = tree nd lf


sumTree :: BTree Integer -> Integer
sumTree = tree (\x y z -> x + y + z) id
-- sumTree t = case t of
--     Node a l r -> a + sumTree l + sumTree r
--     Leaf a -> a

depth :: BTree a -> Int
depth = tree (\ _ y z -> 1 + max y z) (const 1)
--    Node _ l r -> 1 + max (depth l) (depth r)
--    Leaf _ -> 1

myTree2 = tree Node Leaf myTree

myTree = 
    Node
        3
        (Leaf 4)
        (Node 
            1
            (Leaf 7)
            (Leaf 10)
        )



data Nat = Z | Succ Nat
    deriving Show

add :: Nat -> Nat -> Nat
add n k = case k of
    Z -> n
    Succ m -> add (Succ n) m

mul :: Nat -> Nat -> Nat
mul n k = case k of
    Z -> Z
    -- n * (m+1)
    Succ m -> add (mul n m) n

drei = Succ . Succ . Succ $ Z

zwei = Succ . Succ $ Z

fuenf = add drei zwei