\documentclass[xcolor=dvipsnames
%, handout
]{beamer}

\input{../Auxiliary-latex-files/header.tex}

\title{Funktionale Programmierung\\
    \small{Fixpunkte} }
\date{}

\def\tabularxcolumn#1{m{#1}}

\def\kw{10}


\begin{document}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Fixpunkte}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Definition}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
    Fixpunkte bieten einen konstruktiven Zugang zu rekursiv definierten Funktionen\footnote{Das heisst zu verstehen, wieso die verschiedenen Rekursionsgleichungen eine Lösung (die zu definierende Funktion) besitzen und wie diese konstruiert wird.}. Sie formalisieren die Idee des ``Aufbauens einer rekursiven Struktur von Unten''\footnote{Stichwort: Fixpunktiteration}.

\begin{definition}
    Als Fixpunkte einer Funktion $F:X\to Y$ werden Elemente $x\in X\cap Y$ mit
    \begin{align*}
    F(x)=x
    \end{align*}
    bezeichnet.
\end{definition}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
    Unter geeigneten Umständen\footnote{Wenn der Fixpunkt existiert und eindeutig ist.}, lassen sich Definitionen als Fixpunktgleichungen darstellen:
    \begin{align*}
    \text{Das $x$ mit der Eigenschaft: } f(x)=x.
    \end{align*}
    Man kann zum Beispiel die Zahl $0$ durch die Gleichung
    \[
    2x=x
    \]
    definieren.
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Fixpunkte und Rekursion}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\defverbatim[colored]\lstIXV{
\begin{lstlisting}[frame=leftline]
expF f x
    | x == 0 = 1
    | otherwise = 2 * f (x - 1)
\end{lstlisting}
}
\begin{fr}
    Wir betrachten nun Fixpunkte von Funktionen höherer Ordnung\footnote{In diesem Zusammenhang oft Funktionale genannt.}. Wir beginnen mit dem Beispiel
    \begin{align*}
    \mathsf{expF}(f) = x\mapsto \begin{cases}
    1 &\text{falls }x = 0\\
    2\cdot f(x - 1)&\text{sonst.}
    \end{cases}
    \end{align*}
    Oder äquivalent dazu
    \lstIXV
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\defverbatim[colored]\lstIXVb{
\begin{lstlisting}[frame=leftline]
exp x
    | x == 0 = 1
    | otherwise = 2 * exp (x - 1)
\end{lstlisting}
}
\begin{fr}
    Wenn wir das Funktional $\mathsf{expF}$ mit der rekursiven Definition der Funktion $x\mapsto 2^x$ vergleichen, dann sehen wir deutliche Parallelen:
    \lstIXV
    \lstIXVb
    Konkret besteht der einzige Unterschied darin, dass in $\mathsf{expF}$ der Parameter $f$ anstelle eines rekursiven Aufrufes steht.
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
    Unsere Vermutung könte also sein, dass ein Fixpunkt von $\mathsf{expF}$ gerade der Funktion $x\mapsto 2^x$ entspricht (weil dann $f=\mathsf{expF} f$ gilt).

    Gehen wir nun davon aus, dass wir einen Fixpunkt $H$ von $\mathsf{expF}$ haben. Es gelte also
    \begin{align*}
    \mathsf{expF}(H) = H
    \end{align*}
    und deshalb
    \begin{align*}
    \mathsf{expF}(H)(x) = H(x).
    \end{align*}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
    Wir untersuchen nun das Verhalten von $H$..
    \begin{align*}
    H(0)&=\mathsf{expF}(H)(0)= 1\\
    H(n+1) &= \mathsf{expF}(H)(n+1)=2\cdot H(n)
    \end{align*}
    \pause... und erkennen, dass $H(x)=2^x$ gilt.
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
    Natürlich ist das von uns beobachtete Phänomen nicht auf die Exponentialfunktion beschränkt, vielmehr handelt es sich dabei um eine Technik die es erlaubt, wenn die nötigen Voraussetzungen erfüllt sind, Lösungen für beliebige Rekursionsgleichungen zu finden. Wir werden dies nun exemplarisch für das Schema der primitiven Rekursion betrachten.
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
    Es sei die Funktion $f$ wie folgt definiert.
    \begin{align*}
        f(0,\vec{x}) &= c(\vec{x})\\
        f(n+1,\vec{x})&=g(f(n,\vec{x}),n,\vec{x})
    \end{align*}
    Wir betrachten ein ``dazu passendes (nicht rekursives!) Funktional'' $F$:
    \begin{align*}
        F(f)(0,\vec{x}) &= c(\vec{x})\\
        F(f)(n+1,\vec{x}) &= g(f(n,\vec{x}),n,\vec{x})
    \end{align*}
    und einen Fixpunkt $H$ von $F$:
    \begin{align*}
        F(H) = H
    \end{align*}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
    Es folgt:
    \begin{align*}
        H(0,\vec{x}) = F(H)(0,\vec{x}) = c(\vec{x})
    \end{align*}
    und
    \begin{align*}
        H(n+1,\vec{x}) &= F(H)(n+1,\vec{x})\\
        &= g(H(n,\vec{x}),n,\vec{x}).
    \end{align*}
    Insgesamt erfüllt also $H$ die Rekursionsgleichungen von $f$ und es gilt somit $f=H$.
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\defverbatim[colored]\lstFix{
\begin{lstlisting}[frame=leftline]
fix f = f $ fix f
\end{lstlisting}
}
\begin{fr}
    Wir können in Haskell direkt eine Funktion \texttt{fix} zum finden von Fixpunkten definieren:
    \lstFix
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\defverbatim[colored]\lstFixExercise{
\begin{lstlisting}[frame=leftline]
fix f = f $ fix f
\end{lstlisting}
}
\begin{fr}
    \begin{ex}
        Implementieren Sie je ein ein Funktional für die Fibonacci Funktion und für die Collatz Funktion.
    \end{ex}
\end{fr}
\end{document}
