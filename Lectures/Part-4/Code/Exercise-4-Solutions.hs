------------------------------------------------------------
-- Ex. 1
------------------------------------------------------------

prim c g 0 x = c x
prim c g n x = g (f (n-1) x) (n-1) x
    where
        f = prim c g

m2 :: Integer -> () -> Integer
m2 = prim (const 0) (\r _ _ -> 2+r)

e2 :: Integer -> () -> Integer
e2 = prim (const 1) (\r _ _ -> 2*r)

exp_ :: Integer -> Integer -> Integer
exp_ = prim (const 1) (\r _ x -> x*r)

fact :: Integer -> () -> Integer
fact = prim (const 1) (\r n _ -> (n+1) * r)

------------------------------------------------------------
-- Ex. 2
------------------------------------------------------------
-- Nein, Nein, Ja

------------------------------------------------------------
-- Ex. 3
------------------------------------------------------------

sieve :: (a -> a -> Bool) -> [a] -> [a]
sieve pred xs = case xs of
    [] -> []
    x : xs -> x:(sieve pred $ filter (pred x) xs)

sieveAcc :: (a -> a -> Bool) -> [a] -> [a]
sieveAcc pred = reverse . sieve_ []
    where
        sieve_ acc [] = acc
        sieve_ acc (x:xs) =
            sieve_ (x:acc) $ filter (pred x) xs

sieveCtn :: (a -> a -> Bool) -> [a] -> [a]
sieveCtn pred = sieve_ id
    where
        sieve_ ctn [] = ctn []
        sieve_ ctn (x:xs) =
            sieve_ (\c -> ctn (x:c)) $ filter (pred x) xs

primes :: Integer -> [Integer]
primes n = sieveCtn pred [2..n]
    where
        pred x y = not $ y `mod` x == 0

------------------------------------------------------------
-- Ex. 4
------------------------------------------------------------

data Tree a = Tree a [Tree a]

depth :: Tree a -> Integer
depth (Tree _ subtrees) =
    1 + (maximum $ 0:(map depth subtrees))

depthTR :: Tree a -> Integer
depthTR t = depth_ 0 [t]
    where
        cut :: Tree a -> [Tree a]
        cut (Tree _ []) = []
        cut (Tree _ ts) = ts

        depth_ n [] = n
        depth_ n ts = depth_ (n+1) $ concatMap cut ts

------------------------------------------------------------
-- Ex. 5
------------------------------------------------------------

trib :: Integer -> Integer
trib n = trib_ 1 1 1 n
    where
        trib_ x y z 0 = x
        trib_ x y z n = trib_ y z (x+y+z) (n-1)