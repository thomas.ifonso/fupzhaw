exp2 :: Integer -> Integer
exp2 0 = 1
exp2 n = 2 * exp2 (n-1)

expF :: (Integer -> Integer) -> Integer -> Integer
expF _ 0 = 1
expF f n = 2 * f (n - 1) 

{--
h n = expF h n

h 0 = 1
h n = 2 * h n

fr 0 = c
fr x = g (fr (x-1))

F _ 0 = c
F f n = g (f (n-1))

h x = F h x
h 0 = c
h x = g (h (x-1))

--}

-- x = f x

fix f = f (fix f)