import Data.List (sort)

------------------------------------------------------------
-- Aufgabe 1
------------------------------------------------------------

-- (a)

newtype Model = Model String
    deriving (Show, Eq, Ord)

newtype Make = Make String
    deriving (Show, Eq, Ord)

data Color = RGB
    { redPart :: Integer
    , greenPart :: Integer
    , bluePart :: Integer
    } deriving (Show, Eq, Ord)

type Horsepower = Integer

data Car = Car
    { model :: Model
    , make :: Make
    , year :: Integer
    , color :: Color
    , power :: Horsepower
    } deriving (Show, Eq)

-- (b)
ford :: Car
ford = Car (Model "Fiesta") (Make "Ford") 2017 (RGB 255 0 0) 70

ferrari :: Car
ferrari = Car (Model "Testarossa") (Make "Ferrari") 1991 (RGB 0 255 0) 270

zoe :: Car
zoe = Car (Model "Zoé") (Make "Renault") 2023 (RGB 255 255 255) 70

-- (c)
instance Ord Car where
    compare car1 car2 = compare
        (power car1, year car1, make car1, model car1, color car1)
        (power car2, year car2, make car2, model car2, color car2)

-- (d)
unsortedCars :: [Car]
unsortedCars = [ford, ferrari, zoe]

sortedCars :: [Car]
sortedCars = sort [ford, ferrari, zoe]


------------------------------------------------------------
-- Aufgabe 2
------------------------------------------------------------

data Tree a = Node (Tree a) a (Tree a) | Leaf a

-- (a)
collect :: Tree a -> [a]
collect t = case t of
    Leaf a -> [a]
    Node l a r -> a : concat [collect l, collect r]

-- (b)
data WideTree a = WideTree a [WideTree a]

exampleTree :: WideTree Integer
exampleTree = WideTree 1
    [ WideTree 2
        [ WideTree 5 []
        , WideTree 6 []
        , WideTree 7 []
        ]
    , WideTree 3
        [ WideTree 8 []
        ]
    ]

------------------------------------------------------------
-- Aufgabe 3
------------------------------------------------------------

-- (a)
data Nat = Z | S Nat
    deriving Show

-- (b)
eval :: Nat -> Integer
eval Z = 0
eval (S n) = eval n + 1

-- (c)
uneval :: Integer -> Nat
uneval n | n < 0 = error "cannot uneval negative values"
                -- ^ We will discuss better ways to deal
                -- with undefined values later.
         | n == 0 = Z
         | otherwise = S $ uneval (n-1)

-- (d)
add :: Nat -> Nat -> Nat
add Z n = n
add (S n) m = add n (S m)

mul :: Nat -> Nat -> Nat
mul Z n = Z
mul (S n) m = mul n m `add` m

fact :: Nat -> Nat
fact Z = S Z
fact (S n) = S n `mul` fact n

------------------------------------------------------------
-- Aufgabe 4
------------------------------------------------------------

-- Following is a very simple interpretation. Feel free to
-- use more expessive types.

-- Fraction = (numerator, denominator)
type Fraction = (Integer, Integer)

type Program = [Fraction]

type Input = Integer

type Output = [Integer]

execute :: Program -> Input -> Output
execute program input = reverse $ execute_ program input []
    where
        execute_ :: Program -> Input -> Output -> Output
        execute_ [] input output = input:output
        execute_ ((n,d):ps) input output
            | input*n `mod` d == 0 = execute_ program
                (input*n `div` d) (input:output)
            | otherwise = execute_ ps input output

fibProgram :: Program
fibProgram =
    [ (91,33)
    , (11,13)
    , (1,11)
    , (399,34)
    , (17,19)
    , (1,17)
    , (2,7)
    , (187,5)
    , (1,3)
    ]

-- Should evaluate to True
shouldBeTrue = sum (execute fibProgram 31250) == 26183978971946924
