

{-
    This file is dedicated to the fact that every Monad is also an Applicative Functor
    and every Applicative Functor is also a Monad.

    ## Signatures

    - return/pure :: a -> m a
    - <$>/fmap :: (a -> b) -> m a -> m b
    - <*> :: m (a -> b) -> m a -> m b
    - >>= :: m a -> (a -> m b) -> m b

    ## Functor Laws

    F.identity: id <$> a == a
    F.composition: (f . g) <$> a == f <$> (g <$> a)

    ## Applicative Laws

    A.identity: pure id <*> a == a
    A.Composition: pure (.) <*> f <*> g <*> a == f <*> (g <*> a)
    A.Homomorphism: pure f <*> pure a == pure (f a)
    A.Interchange: f <*> pure a == pure ($ a) <*> f

    # Monad Laws

    M.“left identity”: pure a >>= f == f a
    M.“Right identity”: m >>= pure == m
    M.Associativity: (m >>= f) >>= g == m >>= (\x -> f x >>= g)

    # Every Monad is a Functor

    Define: f <$> a = a >>= return .  f
    F.identity: id <$> a = a >>= return . id
                         = a >>= return
                         = a                    // M."Right Identity"

    F.Composition: (f . g) <$> a = a >>= return . (f . g)
                                 = a >>= (return . f) . g
                                 =


                   f <$> (g <$> a) = (g <$> a) >>= return . f
                                   = (a >>= return . g) >>= return . f
                                   = a >>= (\x -> return . g x >>= f)
                                   = a >>= (\x -> pure (g x) >>= f)
                                   = a >>= (\x -> f (g x))              // M."Left Identity"
                                   = a >>= (f.g)
-}

-- Example

newtype A a = A a
    deriving Show

instance Monad A where
    return = A
    A a >>= f = f a

-- Every Monad is a Functor
instance Functor A where
    fmap f a = a >>= return . f

-- Every Monad is an Applicative
instance Applicative A where
    pure = return
    f <*> x = x >>= (\a -> ($ a) <$> f)

{--
-- Every Applicative is a Functor
instance Functor A where
    fmap f a = pure f <*> a
--}