data Tree a = Tree a [Tree a]



depth :: Int -> [Tree a] -> Int
depth n [] = n
depth n ts = depth (n+1) $ concatMap subtrees ts
    where
        subtrees :: Tree a -> [Tree a]
        subtrees (Tree _ ts) = ts

t :: Tree Int
t = Tree 1
    [ Tree 2 []
    , Tree 3
        [ Tree 4 []
        , Tree 5 []
        , Tree 6 []
        ]
    ]

tribonacci :: Integer -> Integer
tribonacci = tri 1 1 1
    where
        tri :: Integer -> Integer -> Integer -> Integer -> Integer
        tri a b c n | n < 3 = a
                    | otherwise = tri (a+b+c) a b (n-1)