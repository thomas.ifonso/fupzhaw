import Data.Functor.Contravariant

------------------------------------------------------------
-- Exercise 1
------------------------------------------------------------


newtype Boxed a = Boxed {unbox :: a}

instance Functor Boxed where
    fmap f (Boxed x) = Boxed $ f x


{-
Functor law 1:
id <$> Boxed x = Boxed (id x) = Boxed x


Functor law 2:
(f . g) <$> Boxed x = Boxed $ (f . g) x
                    = Boxed $ (f (g x))
                    = f <$> (Boxed (g x))
                    = f <$> (g <$> Boxed x)
-}

------------------------------------------------------------
-- Exercise 2
------------------------------------------------------------

newtype FromInt a = FromInt {fun :: Int -> a}

-- (a)
instance Functor FromInt where
    fmap f fi = FromInt $ f . (fun fi)

--(b)
{-
Functor law 1:
id <$> FromInt f = FromInt (id . f)
                 = FromInt f

Functor law 2:
consider:
left side =
    (f . g) <$> fi = FromInt $ (f . g) . (fun fi)
                   = FromInt $ f . (g . (fun fi))
                   = FromInt $ f . (g . (fun fi))

right side =
    f <$> (g <$> fi) = f <$> (FromInt (g . (fun fi)))
                     = FromInt $ f . (fun (FromInt (g. (fun fi))))
                     = FromInt $ f . (g . (fun fi))

thus, q.e.d
-}

------------------------------------------------------------
-- Exercise 3
------------------------------------------------------------

newtype MyPredicate a = MyPredicate (a -> Bool)

instance Contravariant MyPredicate where
    contramap f (MyPredicate g) = MyPredicate $ g . f