## Every "Monad is a Functor"

**We want**:

```
(<$>) :: (a -> b) -> m a -> m b
```

such that

```
-- identity
id <$> x == x

-- Composition
f . g <$> x == f <$> (g <$> x)
```

**We have**: A Monad

```
(>>=) :: m a -> (a -> m b) -> m b
```
such that

```
-- Left identity:
pure a >>= h == h a

-- Right identity:
m >>= pure == m

-- Associativity:
(m >>= g) >>= h == m >>= (\x -> g x >>= h)
```

## Defining 'map' with 'bind'

```
-- Definition
f <$> m = m >>= pure . f
```
## Checking the Functor laws

```
-- Identity
id <$> m >>= pure . id
  == m >>= pure
  == m // By Monad Law 'Right identity'
```

```
-- Composition
 f <$> (g <$> m)
  == (g <$> m) >>= pure . f
  == (m >>= pure . g) >>= pure . f
  == m >>= (\x -> pure (g x) >>= pure . f)
  == m >>= (\x -> pure (f (g x)))
  == m >>= (pure . (f . g))
  == (f . g) <$> m
```